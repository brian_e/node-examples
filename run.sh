#! /bin/bash

set -eu

./node_modules/.bin/eslint ${1}
./node_modules/.bin/babel-node --presets es2015 -- ${1}
