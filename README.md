# Node.js examples

## Setup

    npm init
    npm install --save-dev babel-cli babel-preset-es2015 eslint
    echo '{presets: ["es2015"]}' > .babelrc
    ./node_modules/.bin/eslint --init

## Run

    ./run.sh url-foo.js
